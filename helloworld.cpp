#include <iostream>
#include <queue>
#include <string>

int main() {
    std::cout << "Hello World! From Beegol" << std::endl;
    std::queue<int> Q;
    for(int i = 0; i < 10; i ++) {
        Q.emplace(i);
    }
    while(!Q.empty()) {
      int q = Q.front();
      std::cout << "Pop: " <<  std::to_string(q) << std::endl;
      Q.pop();
    }

    return 0;
}